import { combineReducers } from 'redux'
import authReducer from './authReducer'
import modalReducer from './modalReducer'
import cursoReducer from './cursoReducer'

const rootReducer = combineReducers({
  // set global state
  modal: modalReducer,
  auth: authReducer,
  curso: cursoReducer
  
})

export default rootReducer
