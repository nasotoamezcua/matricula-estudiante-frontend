import { ESTUDIANTE_ENDPOINT } from '../core/appConstants'
import baseApi from './baseApi'

const getEstudianteUrl = (id) => `${ESTUDIANTE_ENDPOINT}/${id}`

class EstudianteService{

    static fetchEstudiantes = () => baseApi.get(ESTUDIANTE_ENDPOINT);

    static fetchEstudiantesOrderByEdad = () => baseApi.get(`${ESTUDIANTE_ENDPOINT}/orderByEdad`);

    static fetchEstudiante = (id) => baseApi.get(getEstudianteUrl(id));

    static addEstudiante = (estudiante) => baseApi.post(ESTUDIANTE_ENDPOINT, estudiante);

    static updateEstudiante = (estudiante) => baseApi.put(ESTUDIANTE_ENDPOINT, estudiante);

    static deleteEstudiante = (id) => baseApi.delete(getEstudianteUrl(id));

}

export default EstudianteService 