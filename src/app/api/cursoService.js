import { CURSO_ENDPOINT } from '../core/appConstants'
import baseApi from './baseApi'

const getCursoUrl = (id) => `${CURSO_ENDPOINT}/${id}`

class CursoService{
    static fetchCursos = () => baseApi.get(CURSO_ENDPOINT);

    static fetchCurso = (id) => baseApi.get(getCursoUrl(id));

    static addCurso = (curso) => baseApi.post(CURSO_ENDPOINT, curso);

    static updateCurso = (curso) => baseApi.put(CURSO_ENDPOINT, curso);
    
    static deleteCurso = (id) => baseApi.delete(getCursoUrl(id));

}

export default CursoService