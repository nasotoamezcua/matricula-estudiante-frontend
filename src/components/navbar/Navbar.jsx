import React from 'react'
import { Menu, Container, Icon, Dropdown, Image } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { logout } from '../../app/store/actions/authActions'

const mapState = (state) => ({
    currentUser: state.auth.currentUser,
})
  
const actions = {
    logout,
}

const Navbar = ({ currentUser, logout }) => {
    return (
        <Menu fixed='top' inverted>
            <Container>
                <Menu.Item header as={Link} to='/'>
                    <Icon name='book'/>
                    RMEstudiantil
                </Menu.Item>
                <Menu.Item header as={Link} to='/cursos'>
                    <Icon name='clipboard list'/>
                    Cursos
                </Menu.Item>
                <Menu.Item header as={Link} to='/estudiantes'>
                    <Icon name='graduation cap'/>
                    Estudiantes
                </Menu.Item>
                <Menu.Item>
                <Icon name='id card'/>
                    <Dropdown pointing="top left" text="Matriculas">
                        <Dropdown.Menu>
                        <Dropdown.Item text="Agregar Matricula" icon="id badge" as={Link} to='/nuevaMatricula' />
                        <Dropdown.Item text="Lista de Matriculas" icon="address card" as={Link} to='/matriculas'/>
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Item>
                <Menu.Item position="right">
                    <Image avatar spaced="right" src="/assets/user.png" />
                    <Dropdown pointing="top left" text={currentUser.sub}>
                        <Dropdown.Menu>
                        <Dropdown.Item text="Logout" icon="log out" onClick={logout} />
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Item>
            </Container>
        </Menu>
    )
}

Navbar.propTypes = {
    currentUser: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
  }
  
  export default connect(mapState, actions)(Navbar)