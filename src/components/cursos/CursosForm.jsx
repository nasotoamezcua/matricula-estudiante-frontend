import React, { useEffect, useState } from 'react'
import { Form, Header, Button } from 'semantic-ui-react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, composeValidators, isRequired } from 'revalidate'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import TextInput from '../form/TextInput'
import { fetchCurso, addCurso, updateCurso } from '../../app/store/actions/cursoActions'
import ErrorMessage from '../form/ErrorMessage'

const validate = combineValidators({
  nombre: isRequired({ message: 'Ingresa un nombre' }),
  siglas: composeValidators(isRequired({ message: 'Ingresa las siglas' }))(),
})

const actions = {
  fetchCurso,
  addCurso,
  updateCurso,
}

const mapState = (state) => ({
  curso: state.curso.curso,
  loading: state.curso.loadingCurso,
})

const CursosForm = ({ id, curso, fetchCurso, loading, addCurso, updateCurso }) => {
  const [actionLabel, setActionLabel] = useState('Agregar Curso')

  useEffect(() => {
    if (id) {
      fetchCurso(id)
      setActionLabel('Editar Curso')
    } else setActionLabel('Agregar Curso')
  }, [fetchCurso, id])

  const handleCreateorEdit = (values) => {
    if (id) {
      updateCurso(values)
    } else {
      const newCurso = {
        nombre: values.nombre,
        siglas: values.siglas,
        estado: true,
      }
      addCurso(newCurso)
    }
  }

  return (
    <FinalForm
      onSubmit={(values) => handleCreateorEdit(values)}
      initialValues={id && curso}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="pink" textAlign="center" />
          <Field name="nombre" component={TextInput} placeholder="Ingresa el nombre del curso" />
          <Field name="siglas" component={TextInput} placeholder="Ingresa las siglas del curso" />
          {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Valores invalidos" />}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="violet"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

CursosForm.propTypes = {
  id: PropTypes.string,
  curso: PropTypes.object,
  fetchCurso: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  addCurso: PropTypes.func.isRequired,
  updateCurso: PropTypes.func.isRequired,
}

CursosForm.defaultProps = {
  id: null,
  curso: null,
}

export default connect(mapState, actions)(CursosForm)
