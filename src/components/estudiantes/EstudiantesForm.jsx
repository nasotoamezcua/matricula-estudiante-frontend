import React, { useEffect, useState } from 'react'
import { combineValidators, isRequired } from 'revalidate'
import { Form as FinalForm, Field } from 'react-final-form'
import { Button, Form, Header } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import useFetchEstudiante from '../../app/hooks/useFetchEstudiante'
import ErrorMessage from '../form/ErrorMessage'
import TextInput from '../form/TextInput'

const validate = combineValidators({
  nombres: isRequired({ message: 'El nombre es requerido' }),
  apellidos: isRequired({ message: 'El apellido es requerido' }),
  dni: isRequired({ message: 'El dni es requerido' }),
  edad: isRequired({ message: 'La edad es requerida' }),
})

const EstudiantesForm = ({ estudianteId, submitHandler }) => {
  const [actionLabel, setActionLabel] = useState('Agregar Estudiante')
  // Estudiante hook
  const [estudiante, loading] = useFetchEstudiante(estudianteId)

  useEffect(() => {
    if (estudianteId) {
      setActionLabel('Editar Estudiante')
    } else setActionLabel('Agregar Estudiante')
  }, [estudianteId])

  return (
    <FinalForm
      onSubmit={(values) => submitHandler(values)}
      initialValues={estudianteId && estudiante}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="pink" textAlign="center" />
          <Field name="nombres" component={TextInput} placeholder="Ingresa  el Nombre" />
          <Field name="apellidos" component={TextInput} placeholder="Ingresa el Apellido" />
          <Field name="dni" component={TextInput} type="number" placeholder="Ingresa el DNI" />
          <Field name="edad" component={TextInput} type="number" placeholder="Ingresa la Edad" />
          {submitError && !dirtySinceLastSubmit && (
            <ErrorMessage error={submitError} text="Valores invalidos" />
          )}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="violet"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

EstudiantesForm.propTypes = {
  estudianteId: PropTypes.string,
  submitHandler: PropTypes.func.isRequired,
}

EstudiantesForm.defaultProps = {
  estudianteId: null,
}

export default EstudiantesForm
