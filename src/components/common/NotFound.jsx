import React from 'react'
import { Segment, Header, Icon, Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const NotFound = () => {
  return (
    <Segment placeholder>
      <Header icon>
        <Icon name="search" />
        Vaya, hemos buscado en todas partes, pero no hemos podido encontrar esto.
      </Header>
      <Segment.Inline>
        <Button as={Link} to="/cursos" primary>
          Regresar a lista de cursos
        </Button>
      </Segment.Inline>
    </Segment>
  )
}

export default NotFound
