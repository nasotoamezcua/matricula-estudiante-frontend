import React, { useEffect, useState } from 'react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, isRequired } from 'revalidate'
import { Button, Form, Grid, Header, Popup, Table } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import history from '../..'
import ErrorMessage from '../form/ErrorMessage'
import TextInput from '../form/TextInput'
import SelectedInput from '../form/SelectedInput'

import { fetchCursos } from '../../app/store/actions/cursoActions'
import MatriculaService from '../../app/api/matriculaService'
import useFetchEstudiantes from '../../app/hooks/useFetchEstudiantes'

const validate = combineValidators({
  fechaMatricula: isRequired({ message: 'La fecha es requerida' }),
  estudiante: isRequired(''),
  curso: isRequired(''),
})

const actions = {
  fetchCursos,
}

const mapState = (state) => {
  const cursos = []

  if (state.curso.cursos && state.curso.cursos.length > 0) {
    state.curso.cursos.forEach((c) => {
      const curso = {
        key: c.id,
        text: c.nombre,
        sigla: c.siglas,
        value: c.id,
      }
      cursos.push(curso)
    })
  }

  return {
    loading: state.curso.loadingCursos,
    cursos,
  }
}

const MatriculaForm = ({ fetchCursos, cursos, loading }) => {
  const [estudiantes] = useFetchEstudiantes()
  const [listaEstudiantes, setListaEstudiantes] = useState([])
  const [loadingEstudiantes, setLoadingEstudiantes] = useState(true)
  const [items, setItems] = useState([])
  const [item, setItem] = useState(null)

  useEffect(() => {
    if (cursos.length === 0) {
      fetchCursos()
    }
    setLoadingEstudiantes(true)
    if (estudiantes) {
      const listaEstudiantes = []
      estudiantes.forEach((e) => {
        const estudiante = {
          key: e.id,
          text: `${e.nombres} ${e.apellidos}`,
          value: e.id,
        }
        listaEstudiantes.push(estudiante)
      })
      setListaEstudiantes(listaEstudiantes)
      setLoadingEstudiantes(false)
    }
  }, [estudiantes, cursos.length, fetchCursos])

  const handleAgregarCurso = () => {
    const newItems = [...items]
    const listaCursos = [...cursos]
    const index = newItems.findIndex((a) => a.id === item)
    if (index > -1) {
      newItems[index] = {
        id: newItems[index].id,
        name: newItems[index].name,
        sigla: newItems[index].sigla,
      }
      setItems(newItems)
    } else {
      const newItem = {
        id: item,
        name: listaCursos.filter((a) => a.key === item)[0].text,
        sigla: listaCursos.filter((a) => a.key === item)[0].sigla,
      }
      newItems.push(newItem)
    }
    setItems(newItems)
  }

  const handleRemoverCurso = (id) => {
    let updatedItems = [...items]
    updatedItems = updatedItems.filter((a) => a.id !== id)
    setItems(updatedItems)
  }

  const handleAgregarNuevaMatricula = async (values) => {
    const newItems = [...items]
    const cursosForMatricula = newItems.map((item) => {
      return {  id: item.id  }
    })

    const newMatricula = {
      fechaMatricula: values.fechaMatricula,
      estudiante: {id: values.estudiante,},
      cursos: cursosForMatricula,
      estado: true
    }

    try {
      const matricula = await MatriculaService.createMatricula(newMatricula)
      toast.info('La matricula se creo satisfactoriamente')
      history.push(`matricula/${matricula.id}`)
    } catch (error) {
      toast.error(error)
    }
  }

  return (
    <FinalForm
      onSubmit={(values) => handleAgregarNuevaMatricula(values)}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading || loadingEstudiantes}>
          <Header as="h2" content="Agregar Matricula" color="pink" textAlign="center" />
          <Field name="fechaMatricula" component={TextInput} type="date" placeholder="Selecciona la fehca" />
          <Field
            name="estudiante"
            component={SelectedInput}
            placeholder="Selecciona un Estudiante"
            options={listaEstudiantes}
            width="5"
          />
          <Grid columns="2">
            <Grid.Row columns="2">
              <Grid.Column width="5">
                <Field
                  name="curso"
                  component={SelectedInput}
                  placeholder="Selecciona un Curso"
                  options={cursos}
                  width="3"
                  handleOnChange={(e) => setItem(e)}
                />
              </Grid.Column>
              <Grid.Column>
                <Popup
                  inverted
                  content="Agregar Curso"
                  trigger={
                    <Button
                      type="button"
                      loading={submitting}
                      color="violet"
                      icon="plus circle"
                      onClick={handleAgregarCurso}
                      disabled={!item}
                    />
                  }
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {items && items.length > 0 && (
                <Table celled collapsing style={{ marginLeft: '2%' }}>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Curso</Table.HeaderCell>
                      <Table.HeaderCell>Siglas</Table.HeaderCell>
                      <Table.HeaderCell />
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {items.map((item) => (
                      <Table.Row key={item.id}>
                        <Table.Cell>{item.name}</Table.Cell>
                        <Table.Cell textAlign="center">{item.sigla}</Table.Cell>
                        <Table.Cell>
                          <Popup
                            inverted
                            content="Eliminar de la Matricula"
                            trigger={
                              <Button
                                color="red"
                                icon="remove circle"
                                type="button"
                                onClick={() => handleRemoverCurso(item.id)}
                              />
                            }
                          />
                        </Table.Cell>
                      </Table.Row>
                    ))}
                  </Table.Body>
                </Table>
              )}
            </Grid.Row>
          </Grid>
          <br />
          {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Valores Invalidos" />}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine || items.length === 0}
            loading={submitting}
            color="violet"
            content="Agregar Nueva Matricula"
          />
        </Form>
      )}
    />
  )
}

MatriculaForm.propTypes = {
  fetchCursos: PropTypes.func.isRequired,
  cursos: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
}

export default connect(mapState, actions)(MatriculaForm)
