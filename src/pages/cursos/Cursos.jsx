import React, { useEffect } from 'react'
import { Segment, Breadcrumb, Table, Divider, Header, Icon, Popup, Button, Container, Grid } from 'semantic-ui-react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { fetchCursos, deleteCurso } from '../../app/store/actions/cursoActions'
import { openModal } from '../../app/store/actions/modalActions'
import LoadingComponent from '../../components/common/LoadingComponent'
import CursosForm from '../../components/cursos/CursosForm'


const actions = {
    fetchCursos,
    openModal,
    deleteCurso,
  }
  
  const mapState = (state) => ({
    cursos: state.curso.cursos,
    loading: state.curso.loadingCursos,
    loadingCurso: state.curso.loadingCurso,
  })


  const Cursos = ({ fetchCursos, cursos, openModal, loading, loadingCurso, deleteCurso }) => {
    useEffect(() => {
      fetchCursos()
    }, [fetchCursos])
  
    let cursoList = <h4>No existen cursos</h4>
  
    if (cursos && cursos.length > 0) {
      cursoList = (
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="5">Nombre</Table.HeaderCell>
              <Table.HeaderCell width="2">Siglas</Table.HeaderCell>
              <Table.HeaderCell width="2" />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {cursos.map((curso) => (
              <Table.Row key={curso.id}>
                <Table.Cell>{curso.nombre}</Table.Cell>
                <Table.Cell>{curso.siglas}</Table.Cell>
                <Table.Cell>
                  <Popup
                    inverted
                    content="Actualizar curso"
                    trigger={
                      <Button
                        basic
                        color="violet"
                        icon="edit"
                        loading={loadingCurso}
                        onClick={() => {
                          openModal(<CursosForm id={curso.id} />)
                        }}
                      />
                    }
                  />
                  <Popup
                    inverted
                    content="Eliminar curso"
                    trigger={
                      <Button
                        basic
                        color="red"
                        icon="trash"
                        loading={loadingCurso}
                        onClick={() => {
                          deleteCurso(curso.id)
                        }}
                      />
                    }
                  />
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      )
    }
  
    if (loading) return <LoadingComponent content="Cargando Cursos..." />
  
    return (
      <Segment>
        <Breadcrumb size="small">
          <Breadcrumb.Section>Cursos</Breadcrumb.Section>
          <Breadcrumb.Divider icon="right chevron" />
          <Breadcrumb.Section active>Mantenedor Cursos</Breadcrumb.Section>
        </Breadcrumb>
        <Divider horizontal>
          <Header as="h4">
            <Icon name="list alternate outline" />
            Lista de Cursos
          </Header>
        </Divider>
        <Segment>
          <Button
            size="large"
            content="Nuevo Curso"
            icon="clipboard list"
            color="purple"
            onClick={() => {
              openModal(<CursosForm />)
            }}
          />
        </Segment>
        <Container>
          <Grid.Column columns="3">
            <Grid.Column width="3" />
            <Grid.Column width="10">{cursoList}</Grid.Column>
            <Grid.Column width="3" />
          </Grid.Column>
        </Container>
      </Segment>
    )
  }

  Cursos.propTypes = {
    fetchCursos: PropTypes.func.isRequired,
    cursos: PropTypes.array.isRequired,
    openModal: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    loadingCurso: PropTypes.bool.isRequired,
    deleteCurso: PropTypes.func.isRequired,
  }

export default connect(mapState, actions)(Cursos)
