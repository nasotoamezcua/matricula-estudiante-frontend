import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import { Breadcrumb, Container, Divider, Grid, Header, Icon, Segment, Table } from 'semantic-ui-react'

import EstudianteService from '../../app/api/estudianteService'
import CursoService from '../../app/api/cursoService'
import MatriculaService from '../../app/api/matriculaService'
import LoadingComponent from '../../components/common/LoadingComponent'

const MatriculaDetalle = ({ match}) => {

    const [matricula, setMatricula] = useState(null)
    const [loading, setLoading] = useState(false)

    const fetchMatricula = useCallback(async () => {
        try {
            setLoading(true)
            const matricula = await MatriculaService.fetchMatricula(match.params.id)
            if(matricula){
                const estudiante = await EstudianteService.fetchEstudiante(matricula.estudiante.id)
                const cursos = []

                if (matricula.cursos.length > 0) {
                    matricula.cursos.forEach((curso) => {
                        CursoService.fetchCurso(curso.id).then((c) => {
                            if (c) {
                                const cursosItem = {
                                  id: c.id,
                                  nombre: c.nombre,
                                  siglas: c.siglas,
                                }
                                cursos.push(cursosItem)
                              }

                              const matriculaDetale = {
                                id: matricula.id,
                                fechaMatricula: matricula.fechaMatricula,
                                estudiante,
                                cursos,
                              }

                              setMatricula(matriculaDetale)
                        }).catch((error) => toast.error(error))
                    })
                }
            }

            setLoading(false)

        } catch (error) {
            setLoading(true)
            toast.error(error)
        }
    },[match.params.id])

     useEffect(() => {
    fetchMatricula()
  }, [fetchMatricula])


    if (loading) return <LoadingComponent content="Cargando Detalle de la Matricula..." />
    let matriculaDetalledArea = <h4>Detalle Matricula</h4>

    if (matricula) {
        matriculaDetalledArea = (
          <Segment.Group>
            <Segment>
              <Header as="h4" block color="violet">
              <Icon name="graduation cap" />
                Estudiante
              </Header>
            </Segment>
            <Segment.Group>
              <Segment>
                <p>
                  <strong>DNI: </strong>
                  {`${matricula.estudiante.dni}`}
                </p>
                <p>
                  <strong>Nombre: </strong>
                  {`${matricula.estudiante.nombres} ${matricula.estudiante.apellidos}`}
                </p>
              </Segment>
            </Segment.Group>
            <Segment>
              <Header as="h4" block color="violet">
              <Icon name="id badge" />
                Matricula
              </Header>
            </Segment>
            <Segment.Group>
              <Segment>
                <p>
                  <strong>Codigo de Matricula : </strong>
                  {matricula.id}
                </p>
                <p>
                  <strong>Fecha Matricula: </strong>
                  {new Date(matricula.fechaMatricula).toLocaleDateString()}
                </p>
              </Segment>
            </Segment.Group>
            <Segment>
              <Table celled striped color="violet">
                <Table.Header >
                  <Table.Row>
                    <Table.HeaderCell colSpan="2">
                      <Icon name="clipboard list" />
                      Cursos
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Siglas</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {matricula.cursos.length > 0 &&
                    matricula.cursos.map((curso) => (
                      <Table.Row key={curso.id}>
                        <Table.Cell>{curso.nombre}</Table.Cell>                       
                        <Table.Cell>{curso.siglas}</Table.Cell>
                      </Table.Row>
                    ))}
                </Table.Body>
              </Table>
            </Segment>
          </Segment.Group>
        )
      }

      return (
        <Segment>
          <Breadcrumb size="small">
            <Breadcrumb.Section>Matricula</Breadcrumb.Section>
            <Breadcrumb.Divider icon="right chevron" />
            <Breadcrumb.Section as={Link} to="/matriculas">
              Lista de Matriculas
            </Breadcrumb.Section>
            <Breadcrumb.Divider icon="right chevron" />
            <Breadcrumb.Section active>Detalle Matriculas</Breadcrumb.Section>
          </Breadcrumb>
          <Divider horizontal>
            <Header as="h4">
              <Icon name="address card outline" />
              Detalle Matriculas
            </Header>
          </Divider>
          <Container>
            <Grid.Column columns="3">
              <Grid.Column width="3" />
              <Grid.Column width="10">{matriculaDetalledArea}</Grid.Column>
              <Grid.Column width="3" />
            </Grid.Column>
          </Container>
        </Segment>
      )
}

MatriculaDetalle.propTypes = {
    match: PropTypes.object.isRequired,
  }

export default MatriculaDetalle
