import React from 'react'
import { Link } from 'react-router-dom'
import { Breadcrumb, Container, Divider, Grid, Header, Icon, Segment } from 'semantic-ui-react'
import MatriculaForm from '../../components/matriculas/MatriculaForm'

const Matricula = () => {
    return (
        <Segment>
          <Breadcrumb size="small">
            <Breadcrumb.Section>Matricula</Breadcrumb.Section>
            <Breadcrumb.Divider icon="right chevron" />
            <Breadcrumb.Section as={Link} to="/matriculas">
              Lista de Matriculas
            </Breadcrumb.Section>
            <Breadcrumb.Divider icon="right chevron" />
            <Breadcrumb.Section active>Nueva Matricula</Breadcrumb.Section>
          </Breadcrumb>
          <Divider horizontal>
            <Header as="h4">
              <Icon name="address card outline" />
              Nueva Matricula
            </Header>
          </Divider>
          <Container>
            <Grid.Column columns="3">
              <Grid.Column width="3" />
              <Grid.Column width="10">
                <MatriculaForm />
              </Grid.Column>
              <Grid.Column width="3" />
            </Grid.Column>
          </Container>
        </Segment>
      )
}

export default Matricula
