import React, { useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Breadcrumb, Button, Container, Divider, Grid, Header, Icon, Popup, Segment, Table } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import MatriculaService from '../../app/api/matriculaService'
import LoadingComponent from '../../components/common/LoadingComponent'

const Matriculas = ({ history }) => {
    const [matriculas, setMatriculas] = useState(null)
    const [loading, setLoading] = useState(false)

   
    const fetchMatriculas = useCallback(async () => {
      setLoading(true)
      try {
        const matriculas = await MatriculaService.fetchMatriculas()
        if (matriculas) setMatriculas(matriculas)
        setLoading(false)
      } catch (error) {
        setLoading(false)
        toast.error(error)
      }
    }, [])

    useEffect(() => {
      fetchMatriculas()
    }, [fetchMatriculas])

    let matriculasList = <h4>No existen matriculas registradas</h4>

  if (matriculas  && matriculas.length > 0) {
    matriculasList = (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="5">ID</Table.HeaderCell>
            <Table.HeaderCell width="2">Fecha Matricula</Table.HeaderCell>
            <Table.HeaderCell width="3" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {matriculas.map((matricula) =>(
            <Table.Row key={matricula.id}>
              <Table.Cell>{matricula.id}</Table.Cell>
              <Table.Cell>{new Date(matricula.fechaMatricula).toLocaleDateString()}</Table.Cell>
              <Table.Cell>
                <Popup
                  inverted
                  content="Detalle de Matricula"
                  trigger={
                    <Button
                      color="violet"
                      icon="address card outline"
                      onClick={() => {
                        history.push(`/matricula/${matricula.id}`)
                      }}
                    />
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    )
  }

  if (loading) return <LoadingComponent content="Cargando Matriculas..." />

  return (
    <Segment>
      <Breadcrumb size="small">
        <Breadcrumb.Section>Matricula</Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section active>Lista de Matriculas</Breadcrumb.Section>
      </Breadcrumb>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="list alternate outline" />
          Matriculas
        </Header>
      </Divider>
      <Container>
        <Grid.Column columns="3">
          <Grid.Column width="3" />
          <Grid.Column width="10">{matriculasList}</Grid.Column>
          <Grid.Column width="3" />
        </Grid.Column>
      </Container>
    </Segment>
  )
}

Matriculas.propTypes = {
    history: PropTypes.object.isRequired,
}

export default Matriculas
