import React, { useEffect, useState } from 'react'
import { Segment, Breadcrumb, Table, Divider, Header, Icon, Popup, Button, Container, Grid } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { toast } from 'react-toastify'
import { openModal, closeModal } from '../../app/store/actions/modalActions'
import LoadingComponent from '../../components/common/LoadingComponent'
import EstudiantesForm from '../../components/estudiantes/EstudiantesForm'
import EstudianteService from '../../app/api/estudianteService'
import useFetchEstudiantes from '../../app/hooks/useFetchEstudiantes'

const actions = {
  openModal,
  closeModal,
}

const Estudiantes = ({ openModal, closeModal }) => {
  const [estudiantesList, setEstudiantesList] = useState([])
  const [loadingAction, setLoadingAction] = useState(false)
  const [loading, setLoading] = useState(true)

  const [estudiantes] = useFetchEstudiantes()

  useEffect(() => {
    setLoading(true)
    if (estudiantes) {
      setEstudiantesList(estudiantes)
      setLoading(false)
    }
  }, [estudiantes])

  const handleCreateorEdit = async (values) => {
    const estudiantesUpdatedList = [...estudiantesList]
    try {
      if (values.id) {
        const updatedEstudiante = await EstudianteService.updateEstudiante(values)
        const index = estudiantesUpdatedList.findIndex((a) => a.id === values.id)
        estudiantesUpdatedList[index] = updatedEstudiante
        toast.info('Estudiante Actualizado')
      } else {
        const estudiante = {
          nombres: values.nombres,
          apellidos: values.apellidos,
          dni: values.dni,
          edad: values.edad,
        }
        const newEstudiante = await EstudianteService.addEstudiante(estudiante)
        estudiantesUpdatedList.push(newEstudiante)
        toast.success('Estudiante Registrado')
      }
      setEstudiantesList(estudiantesUpdatedList)
    } catch (error) {
      toast.error(error)
    }
    closeModal()
  }

  const handleDeleteEstudiante = async (id) => {
    setLoadingAction(true)
    try {
      let estudiantesUpdatedList = [...estudiantesList]
      await EstudianteService.deleteEstudiante(id)
      estudiantesUpdatedList = estudiantesUpdatedList.filter((a) => a.id !== id)
      setEstudiantesList(estudiantesUpdatedList)
      setLoadingAction(false)
      toast.info('Estudiante Eliminado')
    } catch (error) {
      setLoadingAction(false)
      toast.error(error)
    }
  }

  let estudiantesArea = <h4>No existen estudiantes</h4>
  if (estudiantesList && estudiantesList.length > 0) {
    estudiantesArea = (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="4">Nombres</Table.HeaderCell>
            <Table.HeaderCell width="4">Apellidos</Table.HeaderCell>
            <Table.HeaderCell width="3">DNI</Table.HeaderCell>
            <Table.HeaderCell width="2">Edad</Table.HeaderCell>
            <Table.HeaderCell width="4" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {estudiantesList.map((estudiante) => (
            <Table.Row key={estudiante.id}>
              <Table.Cell>{estudiante.nombres}</Table.Cell>
              <Table.Cell>{estudiante.apellidos}</Table.Cell>
              <Table.Cell>{estudiante.dni}</Table.Cell>
              <Table.Cell>{estudiante.edad}</Table.Cell>
              <Table.Cell>
                <Popup
                  inverted
                  content="Actualizar Estudiante"
                  trigger={
                    <Button
                      color="violet"
                      icon="edit"
                      loading={loadingAction}
                      onClick={() => {
                        openModal(<EstudiantesForm estudianteId={estudiante.id} submitHandler={handleCreateorEdit} />)
                      }}
                    />
                  }
                />
                <Popup
                  inverted
                  content="Eliminar Estudiante"
                  trigger={
                    <Button
                      color="red"
                      icon="trash"
                      loading={loadingAction}
                      onClick={() => {
                        handleDeleteEstudiante(estudiante.id)
                      }}
                    />
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    )
  }

  if (loading) return <LoadingComponent content="Loading Customers..." />

  return (
    <>
      <Segment>
        <Breadcrumb size="small">
          <Breadcrumb.Section>Estudiantes</Breadcrumb.Section>
          <Breadcrumb.Divider icon="right chevron" />
          <Breadcrumb.Section active>Mantenedor Estudiantes</Breadcrumb.Section>
        </Breadcrumb>
        <Divider horizontal>
          <Header as="h4">
            <Icon name="list alternate outline" />
            Lista de Estudiantes
          </Header>
        </Divider>
        <Segment>
          <Button
            size="large"
            content="Nuevo Estudiante"
            icon="graduation cap"
            color="purple"
            onClick={() => {
              openModal(<EstudiantesForm submitHandler={handleCreateorEdit} />)
            }}
          />
        </Segment>
        <Container textAlign="center">
          <Grid.Column columns="3">
            <Grid.Column width="3" />
            <Grid.Column width="10">{estudiantesArea}</Grid.Column>
            <Grid.Column width="3" />
          </Grid.Column>
        </Container>
      </Segment>
    </>
  )
}

Estudiantes.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, actions)(Estudiantes)
